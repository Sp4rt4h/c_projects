/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "execute.h"
#include "registers.h"
uint16_t* instruction_pointer;

void execute_rom(uint8_t* rom_memory)
{
	printf("Executing rom...\n");
	//Keep track of what the next instruction is;
	instruction_pointer = (uint16_t*)(rom_memory + 0x200);

	//Just keep looping through each instruction.
	//If we need to stop running, then i will be mutated to 0.
	for(int i = 1, num = 0; 1 == i && *instruction_pointer != 0x0000; num = num + 1) {
		i = execute_instruction(&instruction_pointer);
	}
	
	printf("Finished executing rom...\n");
}

int execute_instruction(uint16_t** current_instruction)
{
	int retval = -1;
	
	//Holds the entire instruction to be executed
	uint16_t instruction = (**current_instruction << 8);
	instruction |= **current_instruction >> 8;

	//Holds the first byte of the instruction, to figure out what we need to do
	uint16_t opcode = **current_instruction & 0xFF;

	

	//Set the instruction pointer to the next instruction
	*current_instruction = *current_instruction + 1;


	printf("opcode is: %x\n", opcode & 0xF0);
	switch(opcode & 0xF0) {				

		case 0x00:	//Clear screen
			if(0x00E0 == instruction) {
				printf("\n\n\n\n\n\n\n\n\n\n\n");
			}
			else {
				printf("calling\n");
			}
			break;

		case 0x60:	//And register with value
			V[opcode & 0x0F] = instruction & 0x00FF;
			break;

		case 0x70:	//Add value to register
			V[opcode & 0x0F] += instruction & 0x00FF;
			break;

		case 0x80:
			printf("%x\n", instruction & 0x000F);
			switch(instruction & 0x000F) {

				case 0x00:	//Set register to register
					V[(instruction & 0x0F00)>>8] = 
					V[(instruction & 0x00F0)>>4];
					break;

				case 0x01:	//Or register with register
					V[(instruction & 0x0F00)>>8] |= 
					V[(instruction & 0x00F0)>>4];
					break;

				case 0x02:	//And register with register
					V[(instruction & 0x0F00)>>8] = 
					V[(instruction & 0x0F00)>>8] &
					V[(instruction & 0x00F0)>>4];
					break;

				case 0x03:	//Xor register with register
					V[(instruction & 0x0F00)>>8] = 
					V[(instruction & 0x0F00)>>8] ^
					V[(instruction & 0x00F0)>>4];
					break;

				case 0x04:
					//Check if the addition will carry
					if(V[(instruction & 0x0F00)>>8] +
					   V[(instruction & 0x00F0)>>4]	>=
					   0x0100) {
						V[0xE] = 1;
					}
					else { //If it won't, unset the c flag
						V[0xE] = 0;
					}

					V[(instruction & 0x0F00)>>8] = 
					V[(instruction & 0x0F00)>>8] +
					V[(instruction & 0x00F0)>>4] ;
					break;
  
				case 0x05:
					//Check if subtraction will borrow
					if(V[(instruction & 0x0F00)>>8] <=
					   V[(instruction & 0x00F0)>>4]	) {
						V[0xF] = 0;
					}
					else { //if it won't, set the b flag
						V[0xF] = 1;
					}

					V[(instruction & 0x0F00)>>8] = 
					V[(instruction & 0x0F00)>>8] -
					V[(instruction & 0x0F00)>>4] ;
					break;

				case 0x06:

					
					break;
				case 0x07:
				case 0x0E:
				default:
					break;
			}
			break;

		default:
			printf("Illegal instruction!\n");
			printf("Opcode is: %X\n", opcode);
			printf("Crashing!\n");
			return retval;
		
	}
	
	retval = 1;

	return retval;
}
