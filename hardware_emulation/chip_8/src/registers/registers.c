/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "registers.h"

uint8_t  V[16];
uint8_t  PC;
uint16_t I;
void dump_registers(void)
{
	printf("Dumping all registers\n");
	for(int i = 0; i < 15; i++)
		printf("V%X: %hhx\n", i, V[i]);
	printf("PC: %hhx\n", PC);
	printf(" I: %hhx\n", I);
}

void clear_registers(void)
{
	for(int i = 0; i < 15; i++)
		V[i] = 0;
	PC = 0;
	I  = 0;
}

