/*
Copyright (C) 2018 Matthew Leon

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "memory.h"

uint8_t memory[4096];

/*This function just dumps every value that is stored in the emulated hardware's memory*/
void dump_memory(void)
{

	printf("Dumping all memory\n");

	for (int i = 0; 1024 > i; i++) {

		printf("%02x %02x %02x %02x ", memory[i * 4], memory[i * 4 + 1],
			memory[i * 4 + 2], memory[i * 4 + 3]);

		if (7 == i % 8)
			printf("\n");
	}

	printf("Done dumping all memory\n");
}

/*This function just sets each byte in memory to be 0*/
void clear_memory(void)
{

	printf("Clearing memory\n");

	for (int i = 0; 4096 > i; i = i + 1) {

		memory[i] = 0;
	}

	printf("Done clearing memory\n");
}
